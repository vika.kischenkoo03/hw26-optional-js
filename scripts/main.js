let offset = 0;
const sliderLine = document.querySelector(".slider_line");

document.querySelector(".second_btn").addEventListener("click", function(){
   offset += 300;
   if (offset > 1500){
    offset = 0;
   }
   sliderLine.style.left = -offset + 'px';
});

document.querySelector(".first_btn").addEventListener("click", function(){
    offset -= 300;
    if (offset < 0){
     offset = 1500;
    }
    sliderLine.style.left = -offset + 'px';
 });